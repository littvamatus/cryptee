//
//  DebugLogger.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import Foundation

final class DebugLogger {
    
    enum LogLevel {
        case error
        case info
        case verbose
        case none
    }
    
    init(logLevel: LogLevel = .verbose) {
        self.logLevel = logLevel
    }
    
    private let logLevel: LogLevel
    
    /// Returns true in case of non release build.
    let isDebug: Bool = {
        var isDebug = false
        // function with a side effect and Bool return value that we can pass into assert()
        func set(debug: Bool) -> Bool {
            isDebug = debug
            return isDebug
        }
        // assert:
        // "Condition is only evaluated in playgrounds and -Onone builds."
        // so isDebug is never changed to true in Release builds
        assert(set(debug: true))
        return isDebug
    }()
    
    func logRequest(_ request: URLRequest) {
        guard isDebug else { return }
        
        log()
        if let url = request.url?.absoluteString.removingPercentEncoding, let method = request.httpMethod {
            logInfo("🚀 Request: \(method) \(url)")
        }
        
        if let httpBody = request.httpBody?.jsonString, !httpBody.isEmpty {
            logVerbose("🎁 Request body:")
            logVerbose(httpBody)
        }
    }
    
    func logResponse(_ response: URLResponse?, data: Data?, error: Error?) {
        guard isDebug, let response = response as? HTTPURLResponse, let url = response.url else { return }
        
        switch response.statusCode {
        case 200 ..< 300:
            logInfo("✅ Response code: \(response.statusCode) \(url)")
        default:
            logInfo("❌ Response code: \(response.statusCode) \(url)")
        }
        
        if let response = data?.jsonString, !response.isEmpty {
            logVerbose("📦 Response JSON:")
            logVerbose("\(response)")
        } else if let data = data, let response = String(data: data, encoding: .utf8) {
            logVerbose("📦 Raw response:")
            logVerbose("\(response)")
        }
        
        if let error = error as NSError? {
            logError("[\(error.domain) \(error.code)] \(error.localizedDescription)")
        } else if let error = error {
            logError("\(error)")
        }
    }
    
    func log(_ string: String = "") {
        if isDebug {
            print(string)
        }
    }
    
    func logError(_ text: String) {
        if logLevel != .none {
            log("‼️ \(text)")
        }
    }
    
    private func logInfo(_ text: String) {
        if logLevel != .error || logLevel != .none {
            log(text)
        }
    }
    
    private func logVerbose(_ text: String) {
        if logLevel == .verbose {
            log(text)
        }
    }
}

private extension Data {
    
    var jsonString: String? {
        do {
            let object = try JSONSerialization.jsonObject(with: self, options: .allowFragments)
            guard JSONSerialization.isValidJSONObject(object) else { return nil }
            let data = try JSONSerialization.data(withJSONObject: object, options: .prettyPrinted)
            return String(data: data, encoding: .utf8)?.replacingOccurrences(of: "\\/", with: "/")
        } catch {
            return nil
        }
    }
}
