//
//  CryptoListCoordinator.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import UIKit

final class CryptoListCoordinator: Coordinator {
    
    private var rootViewController: UINavigationController!
    private let cryptoProvider: CryptoProviderProtocol
    private var detailViewCoordinator: CryptoDetailCoordinator!
    
    init(cryptoProvider: CryptoProviderProtocol) {
        self.cryptoProvider = cryptoProvider
    }
    
    func start() -> UIViewController {
        let viewModel = CryptoListViewModel(provider: cryptoProvider)
        viewModel.coordinatorDelegate = self
        let cryptoList = CryptoListViewController.instantiate(withViewModel: viewModel)
        rootViewController = UINavigationController(rootViewController: cryptoList)
        return rootViewController
    }
}

extension CryptoListCoordinator: CryptoListViewModelCoordinatorDelegate {
    
    func showDetail(asset: CryptoAsset) {
        detailViewCoordinator = CryptoDetailCoordinator(navigationController: rootViewController, data: asset)
        rootViewController.pushViewController(detailViewCoordinator.start(), animated: true)
    }
}
