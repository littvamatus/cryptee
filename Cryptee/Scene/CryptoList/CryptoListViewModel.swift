//
//  CryptoListViewModel.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import Foundation

protocol CryptoListViewModelType {
    var title: String { get }
    var tableModel: [AbstractCellModel] { get }
    
    var loadingHandler: ((Bool) -> Void)? { get set }
    var reloadHandler: (() -> Void)? { get set }
    
    func viewDidLoad()
    func reloadData()
    func loadMoreIfNeeded(row: Int)
    func rowSelected(row: Int)
}

protocol CryptoListViewModelCoordinatorDelegate: AnyObject {
    func showDetail(asset: CryptoAsset)
}

final class CryptoListViewModel: CryptoListViewModelType {
    
    var title: String { "Home" }
    var tableModel: [AbstractCellModel] = []
    
    // MARK: - Handlers
    
    var loadingHandler: ((Bool) -> Void)?
    var reloadHandler: (() -> Void)?
    
    // MARK: - Private
    
    private var isLoading = false {
        didSet {
            loadingHandler?(isLoading)
        }
    }
    private var isAllDataLoaded = false
    private var currentPage = 0
    private var assets: [CryptoAsset] = [] {
        didSet {
            tableModel = assets.map { CryptoListCellModel(asset: $0) }
        }
    }
    
    private let loadMoreOffset = 3
    private let provider: CryptoProviderProtocol
    weak var coordinatorDelegate: CryptoListViewModelCoordinatorDelegate?
        
    init(provider: CryptoProviderProtocol) {
        self.provider = provider
    }

    func viewDidLoad() {
        fetchData()
    }
    
    func reloadData() {
        isAllDataLoaded = false
        currentPage = 0
        fetchData(isRefresh: true)
    }
    
    func loadMoreIfNeeded(row: Int) {
        guard !isAllDataLoaded, row == tableModel.count - loadMoreOffset else { return }
        currentPage += 1
        fetchData()
    }
    
    func rowSelected(row: Int) {
        coordinatorDelegate?.showDetail(asset: assets[row])
    }
    
    // MARK: - Private
    
    private func fetchData(isRefresh: Bool = false) {
        if tableModel.isEmpty {
            isLoading = true
        }
        
        provider.assets(for: currentPage) { [weak self] result in
            switch result {
            case .success(let list):
                self?.updateTableModel(cryptoList: list, isRefresh: isRefresh)
            case .failure(let error):
                break
            }
            self?.isLoading = false
        }
    }
    
    private func updateTableModel(cryptoList: CryptoAssetsList, isRefresh: Bool) {
        // API does not provide a better way to control the end of paging
        isAllDataLoaded = cryptoList.data.isEmpty
        
        if isRefresh {
            assets = cryptoList.data
        } else {
            assets.append(contentsOf: cryptoList.data)
        }
        reloadHandler?()
    }
}
