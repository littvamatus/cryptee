//
//  CryptoListViewController.swift
//  Cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import UIKit

final class CryptoListViewController: UIViewController {

    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    
    private var viewModel: CryptoListViewModelType!
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        return refreshControl
    }()
    
    static func instantiate(withViewModel viewModel: CryptoListViewModelType) -> CryptoListViewController {
        let viewController = instantiate()
        viewController.viewModel = viewModel
        return viewController
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindHandlers()
        viewModel.viewDidLoad()
        setupView()
        setupTableView()
        setupNavigationButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.deselectSelectedRow(along: transitionCoordinator)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        setupNavigationButtons()
    }
    
    // MARK: - Private
    
    private func setupView() {
        title = viewModel.title
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .automatic
        navigationController?.navigationBar.sizeToFit()
    }

    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCells([CryptoListTableViewCell.self])
        tableView.tableFooterView = UIView()
        tableView.refreshControl = refreshControl
    }
    
    private func setupNavigationButtons() {
        guard #available(iOS 13.0, *) else { return }
        let image = UIImage(systemName: traitCollection.userInterfaceStyle == .dark ? "sun.max.fill" : "moon.fill")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(toggleSkin))
    }
    
    @available(iOS 13.0, *)
    @objc
    private func toggleSkin() {
        UIApplication.shared.updateUserInterfaceStyle(traitCollection.userInterfaceStyle == .dark ? .light : .dark)
    }
    
    @objc
    private func reloadData() {
        viewModel.reloadData()
    }
    
    private func bindHandlers() {
        viewModel.loadingHandler = { [weak self] isLoading in
            isLoading ? self?.activityIndicator.startAnimating() : self?.activityIndicator.stopAnimating()
        }
        viewModel.reloadHandler = { [weak self] in
            self?.tableView.reloadData()
            self?.refreshControl.endRefreshing()
        }
    }
}

extension CryptoListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.tableModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = viewModel.tableModel[indexPath.row]
        let cell = tableView.dequeueReusableCell(fromClass: model.cellType, for: indexPath)
        cell.setup(with: model)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.rowSelected(row: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        viewModel.loadMoreIfNeeded(row: indexPath.row)
    }
}
