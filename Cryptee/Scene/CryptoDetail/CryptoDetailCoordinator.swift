//
//  CryptoDetailCoordinator.swift
//  cryptee
//
//  Created by Matus Littva on 26/04/2021.
//

import UIKit

final class CryptoDetailCoordinator: Coordinator {
    
    private let navigationController: UINavigationController
    private let data: CryptoAsset
    
    init(navigationController: UINavigationController, data: CryptoAsset) {
        self.navigationController = navigationController
        self.data = data
    }
    
    func start() -> UIViewController {
        let viewModel = CryptoDetailViewModel(data: data)
        return CryptoDetailViewController.instantiate(withViewModel: viewModel)
        
    }
}
