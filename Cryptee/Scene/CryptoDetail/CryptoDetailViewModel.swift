//
//  CryptoDetailViewModel.swift
//  cryptee
//
//  Created by Matus Littva on 26/04/2021.
//

import Foundation

protocol CryptoDetailViewModelType {
    var title: String { get }
    var name: String { get }
    var percentChange: String { get }
    var value: String { get }
    var imageURL: URL? { get }
    var isNegative: Bool { get }
    var rank: String { get }
    var tableModel: [AbstractCellModel] { get }
    
    var reloadHandler: (() -> Void)? { get set }
    
    func viewDidLoad()
}

final class CryptoDetailViewModel: CryptoDetailViewModelType {
    
    var title: String { data.name }
    var name: String { data.symbol }
    var percentChange: String { data.changePercent24HrDecimal?.formattedPercentage() ?? "-" }
    var value: String { data.priceUsdDecimal?.formatDecimal() ?? "-" }
    var imageURL: URL? { data.symbol == "DOGE" ? URL(string: "https://cutt.ly/0v4Ddly") : data.imageURL }
    var isNegative: Bool { data.changePercent24HrDecimal?.isSignMinus ?? false }
    var rank: String { "Rank #\(data.rank)" }
    var tableModel: [AbstractCellModel] = []
    
    var reloadHandler: (() -> Void)?
    
    private let data: CryptoAsset
    
    init(data: CryptoAsset) {
        self.data = data
    }
    
    func viewDidLoad() {
        setupTableData()
    }
    
    private func setupTableData() {
        if let averagePrice = data.vwap24HrDecimal {
            tableModel.append(TitleValueCellModel(title: "Average price / 24h", value: averagePrice.formatDecimal()))
        }
        if let marketCap = data.marketCapUsdDecimal {
            tableModel.append(TitleValueCellModel(title: "Market Cap", value: marketCap.formatDecimal()))
        }
        if let tradingVolume = data.volumeUsd24HrDecimal {
            tableModel.append(TitleValueCellModel(title: "Trading Volume / 24h", value: tradingVolume.formatDecimal()))
        }
        if let supply = data.supplyDecimal {
            tableModel.append(TitleValueCellModel(title: "Circulating supply", value: "\(supply.formatDecimal(currency: nil, numberStyle: .decimal)) \(data.symbol)"))
        }
        
        reloadHandler?()
    }
}
