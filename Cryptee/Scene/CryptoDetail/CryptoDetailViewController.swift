//
//  CryptoDetailViewController.swift
//  cryptee
//
//  Created by Matus Littva on 26/04/2021.
//

import UIKit
import SDWebImage

final class CryptoDetailViewController: UIViewController {
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var iconImageView: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var valueLabel: UILabel!
    @IBOutlet private var rankLabel: UILabel!
    @IBOutlet private var percentChangeLabel: UILabel!
    @IBOutlet private var percentBackgroundView: UIView!
    
    private var viewModel: CryptoDetailViewModelType!
    
    static func instantiate(withViewModel viewModel: CryptoDetailViewModelType) -> CryptoDetailViewController {
        let viewController = instantiate()
        viewController.viewModel = viewModel
        return viewController
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.viewDidLoad()
        setupTableView()
        setupView()
        setupData()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCells([TitleValueTableViewCell.self])
        tableView.tableFooterView = UIView()
    }
    
    private func setupView() {
        navigationItem.largeTitleDisplayMode = .never
        nameLabel.font = .preferredFont(forTextStyle: .headline)
        nameLabel.textColor = .primaryText
        valueLabel.font = .preferredFont(forTextStyle: .title1)
        valueLabel.adjustsFontSizeToFitWidth = true
        rankLabel.font = .preferredFont(forTextStyle: .footnote)
        rankLabel.textColor = .secondaryText
        percentChangeLabel.font = .preferredFont(forTextStyle: .caption1)
        percentChangeLabel.textColor = .white
        percentBackgroundView.layer.cornerRadius = 4
    }
    
    private func setupData() {
        title = viewModel.title
        iconImageView.sd_setImage(with: viewModel.imageURL, placeholderImage: UIImage(named: "cryptoPlaceholder"))
        nameLabel.text = viewModel.name
        valueLabel.text = viewModel.value
        valueLabel.textColor = viewModel.isNegative ? .red : .positiveGreen
        percentChangeLabel.text = viewModel.percentChange
        rankLabel.text = viewModel.rank
        percentBackgroundView.backgroundColor = viewModel.isNegative ? .red : .positiveGreen
    }
}

extension CryptoDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.tableModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = viewModel.tableModel[indexPath.row]
        let cell = tableView.dequeueReusableCell(fromClass: model.cellType, for: indexPath)
        cell.setup(with: model)
        return cell
    }
}
