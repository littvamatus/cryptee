//
//  ViewController+Instantiable.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import UIKit

protocol Instantiable {
    static func instantiate() -> Self
}

extension Instantiable where Self: UIViewController {
    
    static func instantiate() -> Self {
        let viewControllerName = String(describing: self).replacingOccurrences(of: "ViewController", with: "")
        let storyboard = UIStoryboard(name: viewControllerName, bundle: nil)
        guard let instance = storyboard.instantiateInitialViewController() as? Self else { fatalError("Could not make instance of \(String(describing: self))") }
        return instance
    }
}

extension UIViewController: Instantiable { }
