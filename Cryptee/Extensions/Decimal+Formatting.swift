//
//  Decimal+Formatting.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import Foundation

extension Decimal {
    
    init?(string: String?) {
        guard let string = string, let decimal = Decimal(string: string) else { return nil }
        self = decimal
    }
    
    func formattedPercentage(_ maximumFractionDigits: Int = 2, minimumFractionDigits: Int? = nil) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.minimumIntegerDigits = 1
        formatter.minimumFractionDigits = minimumFractionDigits ?? maximumFractionDigits
        formatter.maximumFractionDigits = maximumFractionDigits
        
        let number = NSDecimalNumber(decimal: self / 100)
        return formatter.string(from: number) ?? "\(description) %"
    }
    
    public func formatDecimal(decimals: Int = 2, zeroDecimals: Int = 4, currency: String? = "$", numberStyle: NumberFormatter.Style = .currency, usesGroupingSeparator: Bool = true) -> String {
        let isZeroValue = self < 1
        let formatter = NumberFormatter()
        formatter.numberStyle = numberStyle
        formatter.minimumIntegerDigits = 1
        formatter.minimumFractionDigits = isZeroValue ? zeroDecimals : decimals
        formatter.maximumFractionDigits = isZeroValue ? zeroDecimals : decimals
        formatter.usesGroupingSeparator = usesGroupingSeparator
        formatter.groupingSize = 3
        formatter.currencySymbol = currency
        
        return formatter.string(from: NSDecimalNumber(decimal: self)) ?? description
    }

}
