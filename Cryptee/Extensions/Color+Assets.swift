//
//  Color+Assets.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import UIKit

extension UIColor {
    
    class var primaryText: UIColor {
        UIColor(named: "primaryText")!
    }
    
    class var secondaryText: UIColor {
        UIColor(named: "secondaryText")!
    }
    
    class var positiveGreen: UIColor {
        UIColor(named: "positiveGreen")!
    }
}
