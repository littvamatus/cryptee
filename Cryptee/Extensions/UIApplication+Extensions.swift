//
//  UIApplication+Extensions.swift
//  cryptee
//
//  Created by Matus Littva on 26/04/2021.
//

import UIKit

public extension UIApplication {
    
    @available(iOS 13.0, *)
    func updateUserInterfaceStyle(_ style: UIUserInterfaceStyle, animated: Bool = true) {
        func overrideInterfaceStyle() {
            windows.forEach {
                $0.overrideUserInterfaceStyle = style
            }
        }
        
        guard animated else {
            overrideInterfaceStyle()
            return
        }
        let snapshotView = UIScreen.main.snapshotView(afterScreenUpdates: false)
        snapshotView.frame = windows.first?.frame ?? .zero
        windows.first?.addSubview(snapshotView)
        
        UIView.animate(withDuration: 0.3) {
            overrideInterfaceStyle()
            snapshotView.alpha = 0
        } completion: { _ in
            snapshotView.removeFromSuperview()
        }
    }
}
