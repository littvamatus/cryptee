//
//  TableView+Extensions.swift
//  cryptee
//
//  Created by Matus Littva on 26/04/2021.
//

import UIKit

extension UITableView {
    
    func deselectSelectedRow(along transitionCoordinator: UIViewControllerTransitionCoordinator?) {
        guard let selectedIndexPath = indexPathForSelectedRow else { return }
        
        guard let coordinator = transitionCoordinator else {
            deselectRow(at: selectedIndexPath, animated: false)
            return
        }
        coordinator.animate { _ in
            self.deselectRow(at: selectedIndexPath, animated: true)
        } completion: { [weak self] context in
            if context.isCancelled {
                self?.selectRow(at: selectedIndexPath, animated: false, scrollPosition: .none)
            }
        }
    }
}
