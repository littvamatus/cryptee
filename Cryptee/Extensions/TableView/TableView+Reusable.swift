//
//  TableView+Reusable.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import UIKit

protocol Reusable {
    static var reuseIdentifier: String { get }
}

extension Reusable where Self: UIView {
    static var reuseIdentifier: String {
        String(describing: self)
    }
}

extension UITableView {
    
    func registerCells(_ cells: [UITableViewCell.Type]) {
        cells.forEach { registerCell(fromClass: $0.self) }
    }
    
    func registerCell<T: UITableViewCell>(fromClass type: T.Type) {
        register(UINib(nibName: type.reuseIdentifier, bundle: nil), forCellReuseIdentifier: type.reuseIdentifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(fromClass type: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: type.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Cell does not exits for \(type).")
        }
        return cell
    }
}

extension UITableViewCell: Reusable {}
