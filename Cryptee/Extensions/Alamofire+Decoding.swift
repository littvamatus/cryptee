//
//  Alamofire+Decoding.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import Alamofire
import Foundation

extension DataRequest {
    
    @discardableResult
    func decode<T: Decodable>(key: String? = nil, decoder: JSONDecoder, logger: DebugLogger, completion: @escaping (Result<T, CrypteeError>) -> Void) -> Self {
        responseData { response in
            if let request = response.request {
                logger.logRequest(request)
                logger.logResponse(response.response, data: self.data, error: self.error?.underlyingError)
            }
            switch response.result {
            case .success(let value):
                if let httpResponse = response.response {
                    switch httpResponse.statusCode {
                    case 200 ..< 300:
                        do {
                            let item = try self.decodeData(value, for: key, decoder: decoder) as T
                            completion(.success(item))
                        } catch let error {
                            let crypteeError = CrypteeError(description: "Error while decoding \(T.self)\n\(error)")
                            logger.logError("‼️ Error while decoding \(T.self)\n\(error)")
                            completion(.failure(crypteeError))
                        }
                    default:
                        let error = CrypteeError(description: "Status code error \(httpResponse.statusCode)")
                        logger.logError(error.description)
                        completion(.failure(error))
                    }
                } else {
                    let error = CrypteeError(description: "Error while decoding \(T.self)")
                    logger.logError(error.description)
                    completion(.failure(error))
                }
            case .failure(let error):
                let error = CrypteeError(description: error.localizedDescription)
                logger.logError(error.description)
                completion(.failure(error))
            }
        }
    }
    
    private func decodeData<T: Decodable>(_ value: Data, for key: String?, decoder: JSONDecoder) throws -> T {
        var item: T?
        
        do {
            if let key = key {
                let nestedItem = try decoder.decode([String: T].self, from: value)
                item = nestedItem[key]
            } else {
                item = try decoder.decode(T.self, from: value)
            }
        } catch {
            throw CrypteeError(description: "Decoding error: \(error)")
        }
        
        if let item = item {
            return item
        } else {
            throw CrypteeError(description: "Key \"\(key ?? "")\" is missing or not decodable.")
        }
    }
}

extension JSONDecoder {
    
    static let defaultDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .millisecondsSince1970
        return decoder
    }()
}
