//
//  Coordinator.swift
//  Cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import UIKit

protocol Coordinator {
    func start() -> UIViewController
}
