//
//  AppCoordinator.swift
//  Cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import UIKit

final class AppCoordinator: Coordinator{
    
    private let logLevel: DebugLogger.LogLevel = .verbose
    private let window: UIWindow
    private var cryptoListCoordinator: CryptoListCoordinator!
    private lazy var cryptoProvider = CryptoProvider(logLevel: logLevel)
    
    init(window: UIWindow) {
        self.window = window
    }
    
    @discardableResult
    func start() -> UIViewController {
        cryptoListCoordinator = CryptoListCoordinator(cryptoProvider: cryptoProvider)
        let mainViewController = cryptoListCoordinator.start()
        window.rootViewController = mainViewController
        window.makeKeyAndVisible()
        return mainViewController
    }
}
