//
//  AbstractCellModel.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import UIKit

protocol AbstractCellModel {
    var cellType: AbstractTableViewCell.Type { get }
}

class AbstractTableViewCell: UITableViewCell {
    func setup(with model: AbstractCellModel) {
        assertionFailure("‼️ Override in superclass ‼️")
    }
}
