//
//  CryptoListTableViewCell.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import UIKit
import SDWebImage

final class CryptoListTableViewCell: AbstractTableViewCell {

    @IBOutlet private var iconImageView: UIImageView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var subtitleLabel: UILabel!
    @IBOutlet private var valueLabel: UILabel!
    @IBOutlet private var percentChangeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    private func setupView() {
        titleLabel.font = .preferredFont(forTextStyle: .title2)
        subtitleLabel.font = .preferredFont(forTextStyle: .footnote)
        valueLabel.font = .preferredFont(forTextStyle: .body)
        percentChangeLabel.font = .preferredFont(forTextStyle: .footnote)
        titleLabel.textColor = .primaryText
        valueLabel.textColor = .primaryText
        subtitleLabel.textColor = .secondaryText
    }
    
    override func setup(with model: AbstractCellModel) {
        guard let model = model as? CryptoListCellModel else { return }
        
        titleLabel.text = model.title
        subtitleLabel.text = model.identifier
        valueLabel.text = model.value
        percentChangeLabel.text = model.percentChange + " \(model.isNegative ? "▼" : "▲")"
        percentChangeLabel.textColor = model.isNegative ? .red : .positiveGreen
        iconImageView.sd_setImage(with: model.imageURL, placeholderImage: UIImage(named: "cryptoPlaceholder"))
    }
}
