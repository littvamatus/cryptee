//
//  CryptoListCellModel.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import UIKit

struct CryptoListCellModel: AbstractCellModel {
    var cellType: AbstractTableViewCell.Type { CryptoListTableViewCell.self }
    
    let title: String
    let identifier: String
    let value: String
    let percentChange: String
    let isNegative: Bool
    let imageURL: URL?
    
    init(asset: CryptoAsset) {
        title = asset.name
        identifier = asset.symbol
        value = asset.priceUsdDecimal?.formatDecimal() ?? "-"
        percentChange = asset.changePercent24HrDecimal?.formattedPercentage() ?? "-"
        isNegative = asset.changePercent24HrDecimal?.isSignMinus ?? false
        imageURL = asset.imageURL
    }
}
