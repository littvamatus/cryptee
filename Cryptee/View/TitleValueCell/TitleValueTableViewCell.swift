//
//  TitleValueTableViewCell.swift
//  cryptee
//
//  Created by Matus Littva on 26/04/2021.
//

import UIKit

final class TitleValueTableViewCell: AbstractTableViewCell {

    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    func setupView() {
        selectionStyle = .none
        titleLabel.font = .preferredFont(forTextStyle: .footnote)
        titleLabel.textColor = .secondaryText
        titleLabel.adjustsFontSizeToFitWidth = true
        valueLabel.font = .preferredFont(forTextStyle: .subheadline)
        valueLabel.textColor = .primaryText
        valueLabel.adjustsFontSizeToFitWidth = true
    }

    override func setup(with model: AbstractCellModel) {
        guard let model = model as? TitleValueCellModel else { return }
        
        titleLabel.text = model.title
        valueLabel.text = model.value
    }
}
