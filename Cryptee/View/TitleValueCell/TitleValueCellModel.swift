//
//  TitleValueCellModel.swift
//  cryptee
//
//  Created by Matus Littva on 26/04/2021.
//

import Foundation

struct TitleValueCellModel: AbstractCellModel {
    var cellType: AbstractTableViewCell.Type { TitleValueTableViewCell.self }
    
    let title: String
    let value: String
}
