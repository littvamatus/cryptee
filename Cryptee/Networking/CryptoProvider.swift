//
//  CryptoProvider.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import Foundation
import Alamofire

protocol CryptoProviderProtocol {
    func assets(for page: Int, completion: @escaping (Result<CryptoAssetsList, CrypteeError>) -> Void)
}

struct CryptoProvider: CryptoProviderProtocol {
    
    private let pageSize = 20
    private let debugLogger: DebugLogger
    private let decoder: JSONDecoder
    
    init(logLevel: DebugLogger.LogLevel, decoder: JSONDecoder = .defaultDecoder) {
        debugLogger = DebugLogger(logLevel: logLevel)
        self.decoder = decoder
    }
    
    func assets(for page: Int, completion: @escaping (Result<CryptoAssetsList, CrypteeError>) -> Void) {
        AF.request(endpoint: .assets(limit: pageSize, offset: page * pageSize)).decode(decoder: decoder, logger: debugLogger, completion: completion)
    }
}
