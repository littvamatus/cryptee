//
//  CryptoEndpoint.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import Foundation
import Alamofire

enum Endpoint: URLConvertible {
    
    case assets(limit: Int, offset: Int)
    
    private static let urlPath = "https://api.coincap.io/v2"
    
    var path: String {
        switch self {
        case .assets:
            return "/assets"
        }
    }
    
    var method: HTTPMethod {
        .get
    }
    
    var parameters: Parameters? {
        switch self {
        case let .assets(limit, offset):
            return ["limit": limit,
                    "offset": offset]
        }
    }
    
    var headers: HTTPHeaders {
        [.accept("application/json"),
         .contentType("application/json")]
    }
    
    var encoding: ParameterEncoding {
        URLEncoding.default
    }
    
    func asURL() throws -> URL {
        var url = try Endpoint.urlPath.asURL()
        url.appendPathComponent(path)
        return url
    }
}

extension Alamofire.Session {
    
    @discardableResult
    func request(endpoint: Endpoint) -> DataRequest {
        request(endpoint,
                method: endpoint.method,
                parameters: endpoint.parameters,
                encoding: endpoint.encoding,
                headers: endpoint.headers).validate()
    }
}

