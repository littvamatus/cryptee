//
//  CryptoAsset.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import Foundation

struct CryptoAssetsList: Decodable {
    let data: [CryptoAsset]
    let timestamp: Date
}

struct CryptoAsset: Decodable {
    
    let id: String
    let rank: String
    let symbol: String
    let name: String
    let explorer: URL?
    
    private let supply: String?
    private let maxSupply: String?
    private let marketCapUsd: String?
    private let volumeUsd24Hr: String?
    private let priceUsd: String?
    private let changePercent24Hr: String?
    private let vwap24Hr: String?
    
    var supplyDecimal: Decimal? { Decimal(string: supply) }
    var maxSupplyDecimal: Decimal? { Decimal(string: maxSupply) }
    var marketCapUsdDecimal: Decimal? { Decimal(string: marketCapUsd) }
    var volumeUsd24HrDecimal: Decimal? { Decimal(string: volumeUsd24Hr) }
    var priceUsdDecimal: Decimal? { Decimal(string: priceUsd) }
    var changePercent24HrDecimal: Decimal? { Decimal(string: changePercent24Hr) }
    var vwap24HrDecimal: Decimal? { Decimal(string: vwap24Hr) }
    
    var imageURL: URL? { URL(string: "https://cryptoicons.org/api/color/\(symbol.lowercased())/128") }
}
