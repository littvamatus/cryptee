//
//  CrypteeError.swift
//  cryptee
//
//  Created by Matus Littva on 25/04/2021.
//

import Foundation

struct CrypteeError: Error, CustomStringConvertible {
    let description: String
}
